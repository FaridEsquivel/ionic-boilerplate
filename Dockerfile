FROM node:12

RUN mkdir /app
WORKDIR /app
RUN npm install -g ionic
COPY package.json /app

RUN npm install 
RUN npm run build:prod

FROM nginx:alpine

COPY --from=0 /app/www/* /usr/share/nginx/html
