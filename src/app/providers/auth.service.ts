import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _isUserAuthenticated = false;

  get isUserAuthenticated() {
    return this._isUserAuthenticated;
  }

  get checkUserAuth() {
    return this.storage.get('uid').then(val => {
      if (val) {
        return true;
      } else {
        return false;
      }
    });
  }

  constructor(
    private fireAuth: AngularFireAuth,
    private storage: Storage
  ) { }

  login(email, password) {
    console.log('Login reached, data is: ', email, password);
    return this.fireAuth.auth.signInWithEmailAndPassword(email, password).then(user => {
      if (user) {
        this.storage.set('uid', user.user.uid);
      }
    });
  }

  createUser(email, password) {
    console.log('Create user reached, data is: ', email, password);
    return this.fireAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  logout() {
    return this.storage.clear();
  }
}
