import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthService } from '../providers/auth.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  isLoading = false;

  constructor(
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    private router: Router,
    private alertController: AlertController,
    private translate: TranslateService
  ) { }

  ngOnInit() {
  }


  onCreateUser(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;
    this.isLoading = true;
    this.loadingCtrl
      .create({ keyboardClose: true, message: 'Creating user...' })
      .then(loadingEl => {
        loadingEl.present();
        this.authService.createUser(email, password).then((user) => {
          loadingEl.dismiss();
          form.reset();
          this.alertController.create({
            header: 'Account created',
            subHeader: 'Your account was created successfully',
            message: 'Now you can log in!',
            buttons: ['OK']
          }).then((ca) => {
            ca.present();
          });
        }).catch(err => {
          loadingEl.dismiss();
          this.alertController.create({
            header: 'Login Failed',
            subHeader: 'Login was not successful',
            message: 'Please verify your email and password.',
            buttons: ['OK']
          }).then((ca) => {
            ca.present();
          });
        });
      });
  }
}
