import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthService } from '../providers/auth.service';
import { Router } from '@angular/router';
import { alertController } from '@ionic/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  isLoading = false;


  constructor(
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    private router: Router,
    private alertController: AlertController,
    private translate: TranslateService
  ) { }

  ngOnInit() {
  }

  choose(event) {
    this.translate.use(event.target.value);
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;
    this.isLoading = true;
    this.loadingCtrl
      .create({ keyboardClose: true, message: 'Logging in...' })
      .then(loadingEl => {
        loadingEl.present();
        this.authService.login(email, password).then((user) => {
          loadingEl.dismiss();
          this.router.navigateByUrl('/home');
          form.reset();
        }).catch(err => {
          loadingEl.dismiss();
          this.alertController.create({
            header: 'Login Failed',
            subHeader: 'Login was not successful',
            message: 'Please verify your email and password.',
            buttons: ['OK']
          }).then((ca) => {
            ca.present();
          });
        });
      });
  }
}
