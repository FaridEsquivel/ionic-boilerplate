// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDS9oJ9ffNd8sAi0j-QnlgtGGWXjND3wmw',
    authDomain: 'places-cc56e.firebaseapp.com',
    databaseURL: 'https://places-cc56e.firebaseio.com',
    projectId: 'places-cc56e',
    storageBucket: 'places-cc56e.appspot.com',
    messagingSenderId: '900890915644',
    appId: '1:900890915644:web:76ca1a7ae94beaa7'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
